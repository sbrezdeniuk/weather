import { useState, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
// Components
import { Filters } from './components/Filters';
import { Head } from './components/Head';
import { CurrentWeather } from './components/CurrentWeather';
import { Forecast } from './components/Forecast';


// Instruments
import { useWeather, useStore } from './hooks';


export const App = observer(() => {
    const { weatherStore } = useStore();
    const { selectedDayId, setSelectedDayId, isFiltered } = weatherStore;
    const { daysWeather } = useWeather();
    const [selectedDay, setSelectedDay] = useState(null);
    const [daysShow, setDaysShow] = useState([]);

    useEffect(() => {
        if (daysShow.length !== 0 && selectedDayId) {
            const searchDay = daysShow.find((day) => day.id === selectedDayId);
            setSelectedDay(searchDay);
        }
    }, [selectedDayId]);

    useEffect(() => {
        if (daysShow.length !== 0) {
            setSelectedDayId(daysShow[ 0 ].id);
            setSelectedDay(daysShow[ 0 ]);
        }
    }, [daysShow]);

    useEffect(() => {
        if (daysWeather.length !== 0 && !isFiltered) {
            setDaysShow(daysWeather);
            setSelectedDayId(daysWeather[ 0 ].id);
        }
    }, [daysWeather, isFiltered]);

    return (
        <main>
            <Filters daysWeather = { daysShow } setDaysShow = { setDaysShow } />
            { daysShow.length ? (
                <>
                    <Head selectedDay = { selectedDay } />
                    <CurrentWeather selectedDay = { selectedDay } />
                    <Forecast daysWeather = { daysShow } />
                </>
            ) : (
                <div className = 'forecast'>
                    <p className = 'message'>По заданным критериям нет доступных дней!</p>
                </div>
            )
            }
        </main>
    );
});
