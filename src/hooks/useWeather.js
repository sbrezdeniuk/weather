import { useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { add, startOfDay, endOfDay } from 'date-fns';

import { api } from '../api';

export const useWeather = () => {
    const [daysWeather, setDaysWeather] = useState([]);
    const query = useQuery('forecast', api.getWeather);
    const { data, isSuccess } = query;

    useEffect(() => {
        if (isSuccess) {
            const startDay = startOfDay(new Date());
            const endDay = endOfDay(add(startDay, { days: 6 }));

            const daysFilter = data?.filter((day) => day.day >= startDay && day.day <= endDay);
            setDaysWeather(daysFilter);
        }
    }, [isSuccess]);

    return {
        daysWeather,
    };
};
