export const CurrentWeather = ({ selectedDay }) => {
    return (
        <div className = 'current-weather'>
            { selectedDay ? (
                <>
                    <p className = 'temperature'>{ selectedDay.temperature }</p>
                    <p className = 'meta'>
                        <span className = 'rainy'>% { selectedDay.rain_probability }</span>
                        <span className = 'humidity'>% { selectedDay.humidity }</span>
                    </p>
                </>
            ) : null }
        </div>
    );
};
