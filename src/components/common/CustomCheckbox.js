export const CustomCheckbox = ({ selectedType, setType, register }) => {
    return (
        <>
            <span className = { `checkbox ${selectedType === 'cloudy' && 'selected'}` }>Облачно
                <input
                    type = 'radio'
                    value = 'cloudy'
                    onClick = { (event) => setType(event.target.value) }
                    { ...register } />
            </span>
            <span className = { `checkbox ${selectedType === 'sunny' && 'selected'}` }>Солнечно
                <input
                    type = 'radio'
                    value = 'sunny'
                    onClick = { (event) => setType(event.target.value) }
                    { ...register } />
            </span>
        </>
    );
};
