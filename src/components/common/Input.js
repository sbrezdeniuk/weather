export const Input = (props) => {
    const input = (
        <input
            type = { props.type }
            { ...props.register } />
    );

    return (
        <p className = 'custom-input'>
            <label>{ props.label }</label>
            { input }
        </p>
    );
};

Input.defaultProps = {
    type: 'number',
    tag:  'input',
};
