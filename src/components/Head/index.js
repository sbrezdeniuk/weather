import { format } from 'date-fns';
import ru from 'date-fns/locale/ru';

export const Head = ({ selectedDay }) => {
    return (
        <div className = 'head'>
            { selectedDay ? (
                <>
                    <div className = { `icon ${selectedDay.type}` }></div>
                    <div className = 'current-date'>
                        <p>{ format(new Date(selectedDay?.day), 'EEEE', { locale: ru }) }</p>
                        <span>{ format(new Date(selectedDay?.day), 'd MMMM', { locale: ru }) }</span>
                    </div>
                </>
            ) : null }
        </div>
    );
};
