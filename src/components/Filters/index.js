import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { observer } from 'mobx-react-lite';

import { useStore } from '../../hooks';
import { Input, CustomCheckbox } from '../common';

export const Filters = observer(({ daysWeather, setDaysShow }) => {
    const { weatherStore } = useStore();
    const {
        applyFilter,
        filteredDays,
        setSelectedDayId,
        isFormBlocked,
        setType,
        setMinTemperature,
        setMaxTemperature,
        resetFilter,
        isFiltered,
    } = weatherStore;
    const form = useForm();
    const [selectedType, setSelectedType] = useState('');

    const onSubmit = form.handleSubmit(async (data) => {
        await applyFilter(data);
        const dayFilter = await filteredDays(daysWeather);
        setSelectedDayId('');
        setDaysShow(dayFilter);
    });

    const handelSelectedType = (chosseType) => {
        setSelectedType(chosseType);
        setType(chosseType);
    };

    const filterReset = () => {
        setSelectedType('');
        resetFilter();
        form.reset();
    };

    return (
        <form className = 'filter' onSubmit = { onSubmit }>
            <CustomCheckbox
                selectedType = { selectedType }
                setType = { handelSelectedType }
                register = { form.register('type', {
                    disabled: isFiltered,
                }) } />
            <Input
                label = 'Минимальная температура'
                setTemperature = { (temp) => setMinTemperature(temp) }
                register = { form.register('minTemperature', {
                    disabled: isFiltered,
                    onChange: (event) => setMinTemperature(event.target.value),
                }) } />
            <Input
                label = 'Максимальная температура'
                register = { form.register('maxTemperature', {
                    disabled: isFiltered,
                    onChange: (event) => setMaxTemperature(event.target.value),
                }) } />
            {
                !isFiltered
                    ? <button type = 'submit' disabled = { isFormBlocked }>Отфильтровать</button>
                    : <button onClick = { filterReset }>Сбросить</button>
            }
        </form>
    );
});
