import { format } from 'date-fns';
import ru from 'date-fns/locale/ru';
import { observer } from 'mobx-react-lite';

import { useStore } from '../../hooks';

export const Day = observer((props) => {
    const { weatherStore } = useStore();
    const { setSelectedDayId, selectedDayId } = weatherStore;
    const {
        id, type, day, temperature,
    } = props.forecastDay;

    const handleSelectedDay = () => {
        setSelectedDayId(id);
    };


    return (
        <div className = { `day ${type} ${selectedDayId === id ? 'selected' : ''}` } onClick = { handleSelectedDay }>
            <p>{ format(new Date(day), 'EEEE', { locale: ru }) }</p>
            <span>{ temperature }</span>
        </div>
    );
});

