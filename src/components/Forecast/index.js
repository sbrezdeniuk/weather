import { Day } from './Day';

export const Forecast = ({ daysWeather }) => {
    return (
        <div className = 'forecast'>
            {
                daysWeather.map((day) => (
                    <Day
                        key = { day.id }
                        forecastDay = { day } />
                ))
            }
        </div>
    );
};
